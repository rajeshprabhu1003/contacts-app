Hi !! This is MyContacts App.

Folder structure - 
1. Inside the 'dist' folder contains prod-ready files bundled using webpack -
    a. index.html       : html file that is opened in browser. 
    a. main.[hash].js   : transpiled and uglyfied/minified code in a single js file.
2. Inside the 'src' folder contains the source-code with the following structure - 
    a. index.html       : contains the view
    b. js/              : contains all the js files
    c. css/             : conatins styles used in this app

Instructions to run the app - 
1. You can run the app by opening 'index.html' in 'dist' folder in your browser.
2. If you want to run the source code i.e. src, please run the following command-
    npm run-script build

Functional Features - 
1. Responsive app for all your screens.
2. All the basic features like add, delete, edit contacts along with search and sort as the additional features.

Non-functional features - 
1. Faster execution of the app due to uglyfied, bundled js code.

Happy Using 😊!!