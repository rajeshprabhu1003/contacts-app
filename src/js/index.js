// css import
import '../css/modal.scss';
import '../css/styles.scss';

// js import
// import jQuery from "jquery";
// window.$ = window.jQuery = jQuery;
let contactList = require("./contactList");
let action = require("./actions");

$(document).ready(function() {
    // initializations
    let modal = document.getElementById("modal");
    let searchValue = null;
    let sortOrder = localStorage.getItem("sortBy");
    $(".datepicker").datepicker({ 
        dateFormat: 'mm-dd-yy',
        changeMonth: true,
        changeYear: true,
        maxDate: '-1D',
        yearRange: '1900:' + new Date().getFullYear()
    });
    let phone = `<div class="phDtl"><select name="type" class="type">
                    <option value="" disabled selected hidden> -- Type -- </option>
                    <option value="home" >Home</option>
                    <option value="office"}>Office</option>
                    <option value="personal" >Personal</option>
                </select>
                <input class="code" type="text" value="" placeholder="Code" maxlength="3"> 
                <input class="phone" placeholder="Enter Phone Number" type="telephone" minlength="10" maxlength="10" name="phone" value="">
                <div class="deletePhone"><i class="fa fa-times"></i></div>
                </div> `;
    let email = `<div class="email-item">
                    <input type="email" class="email-field" placeholder="Enter Email" name="email" value="">
                    <div class="deleteEmail"><i class="fa fa-times"></i></div>
                <div>`;

    // Initial function executions
    populateContacts();

    //function declarations
    function populateContacts(){
        let isSort = contactList.populateContact(sortOrder);
        if(isSort) {
            $("#sortMenu .sortBy").removeClass("active");
            $(`#sortMenu .sortBy[data-sortby="${sortOrder}"]`).addClass("active");
        }
        updateTotalCount();
    }

    function updateTotalCount() {
        // let total = contactList.getCount();
        let total = $("#contact-list .items").not(".hide").length;
        $(".total-contacts .count").text(total);
    }

    function filterContactsList() {
        $("#contact-list .items").filter(function() {
            if($(this).find(".name .data").text().toLowerCase().indexOf(searchValue) > -1) {
                $(this).removeClass("hide");
            } else {
                $(this).addClass("hide");
            }
        });
    }

    function resetForm() {
        $("input[name='fname']").val('').prop( "disabled", false );
        $("input[name='lname']").val('').prop( "disabled", false );
        $("input[name='date']").val('').prop( "disabled", false );
        $("input[name='id']").val('');
        $('.phoneDetails, .emailList').empty();
        $(".phoneDetails").append(phone);
        $(".emailList").append(email);  
        $(".add-phone, .add-email").show();
        $(".deletePhone, .deleteEmail").hide();
        $(".phDtl select, .code, .phone").prop("required", true);
    }

    
    function dateToTimeStamp(date) {
        return date ? new Date(date).getTime() : null;
    }

    function formToJson(form) {
        let id = null;
        if($("input[name='id']").val() != 0){
            id = $("input[name='id']").val();
        } else {
            let list = contactList.getContacts();
            if(list.length > 0){
                id = list[list.length - 1].id + 1;
            } else {
                id = 1;
            }
        }
        let json = {
            id          : +id,
            firstName   : $(form).find("input[name='fname']").val(),
            lastName    : $(form).find("input[name='lname']").val(),
            dob         : dateToTimeStamp($(form).find("input[name='date']").val()),
            phone       : [],
            email       : []
        }

        $(".phoneDetails .phDtl").each(function(index) {
            let type = $(this).children("select.type").val();
            let code = $(this).children("input.code").val();
            let number = $(this).children("input.phone").val();
            if(json.phone.some(item => item.number == number) ){
                alert("Cannot add duplicate Phone numbers");
                return false;
            } else {
                if(type && code && number)
                    json.phone.push({ type, code, number });
            }
        });
        $(".emailList .email-item").each(function(index) {
            let email = $(this).children("input.email-field").val();
            if(json.email.includes(email)){
                alert("Cannot add duplicate Email Ids");
                return false;
            }
            else {
                if(email)
                    json.email.push(email);
            }
        });
        return json;
    }

    // Event Listeners;
    $(".view-buttons").on("click", function(event) {
        if ($(this).hasClass('tile')) {
            $(this).addClass('active');
            $('.list').removeClass('active');
            $('#contact-list').removeClass('list').addClass('tile');
        }
        else if($(this).hasClass('list')) {
            $(this).addClass('active');
            $('.tile').removeClass('active');
            $('#contact-list').removeClass('tile').addClass('list');
        }
    });

    $("#search").on("keypress keyup", function(event) {
        searchValue = $(this).val().toLowerCase();
        let key = event.keyCode || event.charCode;
        if(!((key > 64 && key < 91) || (key > 96 && key < 123) || (key == 8 || key == 46))){
            event.preventDefault();
            return false;
        }
        filterContactsList();
        updateTotalCount();
    });

    $("#sortMenu .sortBy").on("click", function(event) {
        let sortBy = $(this).attr("data-sortby");
        let isSorted = contactList.populateContact(sortBy);
        if(isSorted) {
            localStorage.setItem("sortBy", sortBy);
            $("#sortMenu .sortBy").removeClass("active");
            $(this).addClass("active");
            if($("#search").val() != '')
                filterContactsList();
            updateTotalCount();
        }
    });

    $(document).on('keypress','input.code, input.phone',function(event){
        let key = event.keyCode || event.charCode;
        if (key === 8 || key === 46) return true;
        if ( key < 48 || key > 57 ) return false;
        return true;
    });

    $(document).on('click','.deletePhone',function(){
        $(this).closest(".phDtl").remove();
    });
    $(document).on('click','.deleteEmail',function(){
        $(this).closest(".email-item").remove();
    });

    $("input[name='fname'], input[name='lname']").on('keypress', function(event){
        let key = event.keyCode || event.charCode;
        if(!((key > 64 && key < 91) || (key > 96 && key < 123) || (key == 8 || key == 46))) return false
    });

    $(".add-phone").on("click", function(event) {
        $('.phoneDetails').append(phone);
    });

    $(".add-email").on("click", function(event) {
        $('.emailList').append(email);
    });

    $("#addContact").on("click", function(event) {
        openModal("Add Contact");
    });

    $(document).on("click", ".view", function(event) {
        let dataItem = $(this).closest(`.items`);
        let dataId = +dataItem.attr("data-id");
        action.view(dataId);
        $(".modal-footer").addClass("hide");
        openModal("View Contact");
    });

    $(document).on("click", ".edit", function(event) {
        let dataItem = $(this).closest(`.items`);
        let dataId = +dataItem.attr("data-id");
        action.edit(dataId);
        openModal("Edit Contact");
    });

    $(document).on("click", ".delete", function(event) {
        let dataItem = $(this).closest(`.items`);
        let dataId = +dataItem.attr("data-id");
        action.delete(dataId);  
        $(dataItem).remove();
        updateTotalCount();
    });
    

    $("#modal-form").submit(function(event){
        let json = formToJson(this);
        if(!json) return;
        action.add(json);
        populateContacts(); 
        closeModal();
        event.preventDefault();
    });

    $(".modal-close, .cancel-button").on("click", closeModal);

    //Modal functions
    function initModal() {
        $('.modal-body .body-content').html('');
    }

    function openModal(title) {
        $(".modal-title").text(title);
        $("#modal-form input").prop("autocomplete", "off");
        modal.style.display = "block";
    }
    
    function closeModal() {
        modal.style.display = "none";
        $(".modal-footer").removeClass("hide");
        resetForm() ;
    }
    
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
 });