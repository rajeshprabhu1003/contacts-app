function getContacts() {
    return JSON.parse(localStorage.getItem("contacts")) || [];
}

function setContacts(list) {
    let contacts = JSON.stringify(list);
    localStorage.setItem("contacts", contacts);
}

function timestampToDate(timestamp) {
    if(timestamp){
        let newDate = new Date(timestamp);
        let date = newDate.getDate();
        let year = newDate.getFullYear();
        let month = newDate.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        return `${month}-${date}-${year}`;
    }
    return null;
}

function populateModal(action, id) {
    let contacts = getContacts();
    let item = contacts.filter(i => i.id === id);
    item = item[0];
    $("input[name='id']").val(id);
    $("input[name='fname']").val(item.firstName)
    $("input[name='lname']").val(item.lastName)
    $("input[name='date']").val(timestampToDate(item.dob))
    $('.phoneDetails, .emailList').empty();
    for(let i = 0; i < item.phone.length; i++) {
        let type = item.phone[i].type;
        let phoneItem = `<select name="type" class="type">
                            <option value="home" ${type == 'home' ? 'selected' : ''}>Home</option>
                            <option value="office" ${type == 'office' ? 'selected' : ''}>Office</option>
                            <option value="personal" ${type == 'personal' ? 'selected' : ''}>Personal</option>
                        </select>
                        <input class="code" type="text" placeholder="Code" value="${item.phone[i].code}" maxlength="3"> 
                        <input class="phone" type="telephone" placeholder="Enter Phone Number" minlength="10" maxlength="10" name="phone" value="${item.phone[i].number}">
                        ${(i != 0) ? '<div class="deletePhone"><i class="fa fa-times"></i></div>' : ''}`
        let phone = `<div class="phDtl">
                        ${phoneItem}
                    </div> `;
        
        $(".phoneDetails").append(phone);
    }
    let emailField = `<input type="email" class="email-field" placeholder="Enter Email" name="email" value="${item.email[0] || ''}">`
    insertEmailField(emailField);
    for(let i = 1; i < item.email.length-1; i++) {
        let emailField  = `<input type="email" class="email-field" placeholder="Enter Email" name="email" value="${item.email[i]}">
                        <div class="deleteEmail"><i class="fa fa-times"></i></div>`;
        insertEmailField(emailField); 
    }
    if(action == "view") {
        $("input[name='fname'], input[name='lname'], input[name='date']").prop( "disabled", true );
        $("select.type, input.code, input.phone, input.email-field").prop( "disabled", true );
        $(".add-phone, .add-email, .deletePhone, .deleteEmail").hide();
    }
}

function insertEmailField(emailField) {
    let email = `<div class="email-item">
                    ${emailField}
                </div>`;
    $(".emailList").append(email);   
}

module.exports.view = function(id) {
    populateModal('view', id);
}

module.exports.add = function(json) {
    let contacts = getContacts();
    if(contacts && contacts.length && contacts.some(item => item.id == json.id)) {
        let itemIndex = contacts.findIndex(item => item.id == json.id);
        contacts.splice(itemIndex, 1);
    }
    contacts.push(json);
    console.log(contacts);
    setContacts(contacts);
}

module.exports.edit = function(id) {
    populateModal('edit', id);
}

module.exports.delete = function(id) {
    let contacts = getContacts();
    let itemIndex = contacts.findIndex(item => item.id === id);
    contacts.splice(itemIndex, 1);
    setContacts(contacts);
}