
function getContacts() {
    return JSON.parse(localStorage.getItem("contacts")) || [];
}

function timestampToDate(timestamp) {
    if(timestamp){
        let newDate = new Date(timestamp);
        let date = newDate.getDate();
        let year = newDate.getFullYear();
        let month = newDate.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        return `${month}-${date}-${year}`;
    }
    return null;
}

function addListToHtml(list) {
    $("#contact-list").html('');
    let name, phone, email, dob;
    let action  = `<div class="action">
                    <div class="data">
                        <i class="view fa fa-eye" ></i> | 
                        <i class="edit fa fa-edit" ></i> | 
                        <i class="delete fa fa-trash-o"></i>
                    </div>
                </div>`;

    list.map(item => {
        name    = `<div class="name">
                    <div class="label"><i class="fa fa-user"></i></div>
                    <div class="data">${item.firstName} ${item.lastName}</div>
                </div>`;
        dob =   `${item.dob ? `<div class="dob">
                    <div class="label"><i class="fa fa-calendar"></i></div>
                    <div class="data">${timestampToDate(item.dob)}</div>
                </div>` : '' }`;
        phone   = `${item.phone && item.phone.length > 0 ? 
                    `<div class="telephone">
                        <div class="label"><i class="fa fa-phone"></i></div>
                        <div class="data">+${item.phone[0].code} ${item.phone[0].number} ${item.phone.length > 1 ? `<span class="lightText">(${item.phone.length - 1} more)</span>` : ''}</div>
                    </div>` : 
                ''}`;

        email   = `${item.email && item.email.length > 0 ? 
                    `<div class="email">
                        <div class="label"><i>@</i></div>
                        <div class="data">${item.email[0]} ${item.email.length > 1 ? `<span class="lightText">(${item.email.length - 1} more)</span>` : ''}</div>
                    </div>` : 
                ''}`;
        let html = `<li class="items" data-id=${item.id}>
                        ${name}
                        ${dob}
                        ${phone}
                        ${email}
                        ${action}
                    </li>`;
        $("#contact-list").append(html);
    });
}

module.exports.populateContact = function(sortBy) {
    let list = getContacts();
    if(list && list.length > 0){
        switch(sortBy) {
            case 'fn-asc'  : list = list.sort((prev, next) => {
                                if ( prev.firstName.toLowerCase() > next.firstName.toLowerCase() ) return 1;
                                if ( prev.firstName.toLowerCase() < next.firstName.toLowerCase() ) return -1;
                                return 0;
                            });
                             addListToHtml(list);
                             break;

            case 'fn-desc' : list = list.sort((prev, next) => {
                                if ( prev.firstName.toLowerCase() < next.firstName.toLowerCase() ) return 1;
                                if ( prev.firstName.toLowerCase() > next.firstName.toLowerCase() ) return -1;
                                return 0;
                            });
                             addListToHtml(list);
                             break;

            case 'ln-asc'  : list = list.sort((prev, next) => {
                                if ( prev.lastName.toLowerCase() > next.lastName.toLowerCase() ) return 1;
                                if ( prev.lastName.toLowerCase() < next.lastName.toLowerCase() ) return -1;
                                return 0;
                            });
                             addListToHtml(list);
                             break;

            case 'ln-desc' : list = list.sort((prev, next) => {
                                if ( prev.lastName.toLowerCase() < next.lastName.toLowerCase() ) return 1;
                                if ( prev.lastName.toLowerCase() > next.lastName.toLowerCase() ) return -1;
                                return 0;
                            });
                             addListToHtml(list);
                             break;   

            case 'dob-asc' : list = list.sort((prev, next) => {
                                if ( prev.dob < next.dob ) return 1;
                                if ( prev.dob > next.dob ) return -1;
                                return 0;
                            });
                             addListToHtml(list);
                             break; 

            case 'dob-desc': list = list.sort((prev, next) => {
                                if ( prev.dob > next.dob ) return 1;
                                if ( prev.dob < next.dob ) return -1;
                                return 0;
                            });
                             addListToHtml(list);
                             break; 

            default        : addListToHtml(list);
        }
        return true;
    } 
    return false;
}

module.exports.getContacts = getContacts;